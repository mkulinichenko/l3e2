package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person{
    private double drivingExperience;
    public Driver(String fullName, int age, String gender, String phoneNumber, double drivingExperience) {
        super(fullName, age, gender, phoneNumber);
        this.drivingExperience = drivingExperience;
    }

    public double getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(double drivingExperience) {
        this.drivingExperience = drivingExperience;
    }
    @Override
    public String toString(){return "[ "+ getFullName() +" "+ getAge() +" "+ getGender() +" "+ getPhoneNumber() +" "+drivingExperience+" ]";}
}
