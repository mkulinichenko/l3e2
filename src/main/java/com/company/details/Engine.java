package com.company.details;

import com.company.professions.Driver;

public class Engine{
    private double capacity;
    private String manufacturer;

    public Engine(double capacity, String manufacturer) {
        this.capacity = capacity;
        this.manufacturer = manufacturer;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    @Override
    public String toString(){return "[ "+capacity+" "+manufacturer+" ]";}
}
