package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car{
    private double maximumSpeed;

    public SportCar(String carBrand, String carClass, Driver driver, Engine engine, double weight, double maximumSpeed) {
        super(carBrand, carClass, driver, engine, weight);
        this.maximumSpeed = maximumSpeed;
    }

    public double getMaximumSpeed() {
        return maximumSpeed;
    }
    public void setMaximumSpeed(double maximumSpeed) {
        this.maximumSpeed = maximumSpeed;
    }
    @Override
    public void start(){
        System.out.println("Поїхали___");
    }
    @Override
    public void stop(){System.out.println("Зупиняємося___");}
    @Override
    public void turnRight(){
        System.out.println("Поворот направо___");
    }
    @Override
    public void turnLeft(){
        System.out.println("Поворот наліво___");
    }
    @Override
    public String toString(){return getDriver() +"-"+ getEngine() +"- [ "+ getCarClass() +"-"+ getCarBrand() +"-"+"вага "+ getWeight()+"-"+ maximumSpeed +" ]";}
}
