package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car{
    private double carryingCapacity;

    public Lorry(String carBrand, String carClass, Driver driver, Engine engine, double weight, double carryingCapacity) {
        super(carBrand, carClass, driver, engine, weight);
        this.carryingCapacity = carryingCapacity;
    }

    public double getCarryingCapacity() {
        return carryingCapacity;
    }
    public void setCarryingCapacity(double carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }
    @Override
    public void start(){
        System.out.println("Поїхали...");
    }
    @Override
    public void stop(){System.out.println("Зупиняємося...");}
    @Override
    public void turnRight(){
        System.out.println("Поворот направо...");
    }
    @Override
    public void turnLeft(){
        System.out.println("Поворот наліво...");
    }
    @Override
    public String toString(){return getDriver() +"-"+ getEngine() +"- [ "+ getCarClass() +"-"+ getCarBrand() +"-"+"вага "+ getWeight()+"-"+ carryingCapacity +" ]";}
}
