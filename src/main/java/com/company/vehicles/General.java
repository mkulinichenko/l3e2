package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class General {
    public static void main(String[] args) {
        Driver driver = new Driver("Jhon", 43, "male", "01010101", 4.3);
        Engine engine = new Engine(150, "China");
        SportCar car = new SportCar("BMW", "classic", driver, engine, 999, 300);
        System.out.println(car.toString());
        car.stop();
        car.start();
        car.turnLeft();
        car.turnRight();
    }
}
