package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Car{
    private String carBrand;
    private String carClass;
    private Driver driver;
    private Engine engine;
    private double weight;

    public Car(String carBrand, String carClass, Driver driver, Engine engine, double weight) {
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.driver = driver;
        this.engine = engine;
        this.weight = weight;
    }

    public void start(){
        System.out.println("Поїхали");
    }
    public void stop(){System.out.println("Зупиняємося");}
    public void turnRight(){
        System.out.println("Поворот направо");
    }
    public void turnLeft(){
        System.out.println("Поворот наліво");
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    @Override
    public String toString(){return driver+"-"+engine+"- [ "+carClass+"-"+carBrand+"-"+"вага "+weight+" ]";}
}
